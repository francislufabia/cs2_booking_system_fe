console.log("hello from script.js")
//capture the navSession element inside the navbar element
let navItem = document.querySelector('#navSession')

// lets take the access token from the local storage property.
let userToken = localStorage.getItem("token")
console.log(userToken) //for educational purpose only

//lets create a control structure that will determine which elements inside the navbar will be displayed if a userToken is found in the local storage.
//this will display the logout or login button at the navbar
if(!userToken) {
	navItem.innerHTML = 
	`
	<li class="nav-item">
		<a href="./login.html" class="nav-link">Log in</a>
	</li>
	`
}else{
	navItem.innerHTML = 
	`
	<li class="nav-item">
		<a href="./logout.html" class="nav-link">Log Out</a>
	</li>
	`
}





