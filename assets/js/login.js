console.log("hello from LOGIN.JS file");

let loginForm = document.querySelector('#loginUser')


loginForm.addEventListener("submit", (e) => {
	e.preventDefault()
	let email = document.querySelector("#userEmail").value
	console.log(email)
	let password = document.querySelector("#password").value
	console.log(password)

	//no blanks!
	if(email == "" || password == ""){
		alert("Please input your email and/or password.")
	}else{
		fetch('https://pure-beach-95377.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(res => {
			return res.json()
		}).then(data => {
			console.log(data.access)
			if(data.access){
				localStorage.setItem('token', data.access)
				// alert("access key saved on local storage.") //for educational purposes only.
				fetch('https://pure-beach-95377.herokuapp.com/api/users/details', {
					headers: {
						'Authorization': `Bearer ${data.access}`
					}
				}).then(res => {
					return res.json()
				}).then(data => {
					console.log(data)
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)
					console.log("items are set inside the local storage.")
					window.location.replace('./profile.html')
				})
			}else{
				alert("Something went wrong, check your credentials.")
			}
		})
	}
})


