console.log("hello from JS file");

let registerForm = document.querySelector("#registerUser");

registerForm.addEventListener("submit", (event) => {
	event.preventDefault();
	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let userEmail = document.querySelector("#userEmail").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let password = document.querySelector("#password1").value
	let verifyPassword = document.querySelector("#password2").value

	//information validation upon creating a new entry in the database
	//first validation: passwords must match each other.
	if((password !== "" && verifyPassword !== "")&&(verifyPassword === password)&&(mobileNo.length === 11)){
		//the line below is from the emailExists from user.js in routes backend
		fetch('https://pure-beach-95377.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: userEmail
			})
		}).then(res => res.json()
			) //this will give the information if there are no email duplicates found
			.then(data => {
				if(data === false){
					//the lines below will post if there are no duplicate emails (this is the normal posting method)
					fetch("https://pure-beach-95377.herokuapp.com/api/users/register", {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: userEmail,
						mobileNo: mobileNo,
						password: password,
					}) //left variable can be found in the schema, while the right part is found above
				}).then(res => {
					return res.json()
				}).then(data => {
					if (data === true) {
						alert("New account registered successfully!")
						window.location.replace('./login.html')

					} else {
						alert ("Error registering user.")
					}
				})
					// alert("passwords and mobile number met the criteria, create new document.")
				//the lines below will happen if there are duplicate emails found
				}else {
					alert("Email already exists. Please choose another email")
				}
			})
	}else {
		alert("something went wrong  please check your credentials")
	} //the first condition prevents the user from entering blank passwords, second condition states that both passwords must match, third condition states that mobile number must be 11 digits long.
})