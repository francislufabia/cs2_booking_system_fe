console.log("hello from JS")

//idenfity which course it needs to display inside the browser?
//to do this, we are going to use the course ID to identify the correct course properl

let params = new URLSearchParams(window.location.search)
//window.location -> returns a location object with information about the "current" location of the doucment
//.search contains the query string section of the current URL
// URLSearchParams() - this method/constructor creates and returns a URLSearchParams object.
// URLSearchParams describes the interface that defines utility methods to work with the query string of a URL.
// new -> instantiates a user-defined object.

let id = params.get('courseId')
console.log(id)

//lets capture the sections of the HTML bodies
let name = document.querySelector("#courseName")
let desc = document.querySelector("#courseDesc")
let price = document.querySelector("#coursePrice")

fetch(`https://pure-beach-95377.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(data => {
	console.log(data)

	name.innerHTML = data.name
	desc.innerHTML = data.description
	price.innerHTML = data.price
})

