console.log("Hello from JS")


//capturing the adminButton in courses.html
let modalButton = document.querySelector('#adminButton');


//capture the body container in the html which will display the content coming from the database, in this case, its coursesContainer
let container = document.querySelector('#coursesContainer')
let cardFooter;


//lines below is for the addCourse button not to appear if not admin
//we are going to take the value of the isAdmin property from the local storage.
let isAdmin = localStorage.getItem("isAdmin")
//Then after getting, we will execute the code below
if(isAdmin == "false" || !isAdmin){
	//if a user is a regular user, do not show the addcourse button
	modalButton.innerHTML = null;
}else{
	modalButton.innerHTML = `
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
		</div>
	`
}

//displaying the all courses in cards
fetch('https://pure-beach-95377.herokuapp.com/api/courses/').then(res => res.json()).then(data => {
	console.log(data);
	//declare a variable that will display a result in the browser depending on the return
	let courseData;
	//create a control structure that will determine the value that the variable above will hold
	if(data.length < 1){
		courseData = "No course available"
	}else{
		//iterate the courses collection and display each course inside the browser
		courseData = data.map(course => {
			//check the makeup of each element inside the courses collection
				console.log(course._id);

			//control structure that will display different things if admin(course) or regular user(enroll)
			if(isAdmin == "false" || !isAdmin) 
			{
				cardFooter = `
				<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">View course details</a>
				<a href= "" class="btn btn-success text-white btn-block">Enroll</a>
				`
			}else{
				cardFooter = `
				<a href="./editCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Edit Course</a>
				<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Disable Course</a>
				`
			}
			return (
				`
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title text-center"> ${course.name} </h5>
							<p class="card-text text-center">
								${course.description}
							</p>
							<p class="card-text text-center">
								Php ${course.price}
							</p>
							<p class="card-text text-center">
								${course.createdOn}
							</p>							
						</div>
						<div class="card-footer">
							${cardFooter}
						</div> 
					</div>
				</div>
				`
			)//we attached a query string ? in the a href tag which allows us to embed the ID from the database record into the query string. We used so that the course.js can pick the proper course selected.
		}).join("") //line to your left removes the commas. it concatenated all the objects inside the array and convert it to a string data type.
	}
	container.innerHTML = courseData;
})

