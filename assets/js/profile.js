console.log("hello from profile JS file");

//get the access token in the local storage
let token = localStorage.getItem("token")
console.log(token);


//line below will show the profile
let profile = document.querySelector('#profileContainer')
//lets create a control structure that will determine the display if the access token is null or empty
if (!token || token === null){
	//let redirect the user to the login page
	alert("Please sign in Login first");
	window.location.href = "./login.html"
}else{
	// console.log("yes we got a token")//for educational purposes only
	fetch('https://pure-beach-95377.herokuapp.com/api/users/details', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json', 
			'Authorization': `Bearer ${token}`
		}
	}).then(res => res.json())
	.then(data => {
		console.log(data);//for checking purposes only
		profile.innerHTML = `
			<div class="col-md-12">
				<section class="jumbotron my-5">
					<h3 class="text-center">First Name: ${data.firstName}</h3>
					<h3 class="text-center">Last Name: ${data.lastName}</h3>
					<h3 class="text-center">Email: ${data.email}</h3>
					<table class="table">
						<thead>
							<tr>
								<th>Course ID: </th>
								<th>Enrolled on: </th>
								<th>Status: </th>
								<tbody></tbody>
							</tr>
						</thead>
					</table>
				</section>
			</div>
		`
	})
}